jQuery(document).ready(function(){
  jQuery.goup();
});


/////FORM VALIDATION/////
const modform = document.forms.modform;

const name = document.getElementById('mdname');
const phone = document.getElementById('mdphone');

/// NAME VALIDATION ///
name.addEventListener('input', () => {
  name.setCustomValidity('');
  name.checkValidity();
});

name.addEventListener('invalid', () => {
  if (name.value === '') {
    name.setCustomValidity('Пожалуйста, укажите ваше имя')
  }
  else {
    name.setCustomValidity('Имя может содержать только строчные и заглавные буквы. Попробуйте снова')
  }
})


/// PHONE VALIDATION ///
phone.addEventListener('input', () => {
  phone.setCustomValidity('');
  phone.checkValidity();
});

phone.addEventListener('invalid', () => {
  if (phone.value === '') {
    phone.setCustomValidity('Пожалуйста, укажите номер вашего телефона')
  }
  else {
    phone.setCustomValidity('Пожалуйста, введите номер в федеральном формате: +7 или 8, далее ваш номер.')
  }
})


// FORM SUBMIT //
modform.addEventListener('submit', (event) => {
  event.preventDefault();
  $('#callModal .alert').show();
  let mail = new FormData(modform);
  sendMail(mail);
  modform.reset();
  $("#callModal").on("hidden.bs.modal", function () {
    $('#callModal .alert').hide();
  });
})

// sending forms data
const sendMail = (mail) => {
  fetch("/send", {
    method: "post",
    body: mail,
  }).then((response) => {
    return response.text();
  }).then((data) => {
    console.log(data);
  })
};
