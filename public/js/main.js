//GOUP BUTTON
jQuery(document).ready(function(){
  jQuery.goup();
});


/////FORM VALIDATION/////
const fbform = document.forms.fbform;
const modform = document.forms.modform;

const name = Array.from(document.getElementsByName('name'));
const phone = Array.from(document.getElementsByName('phone'));

name.forEach(nameValidation);
phone.forEach(phoneValidation);

/// NAME VALIDATION ///
function nameValidation(n){
  n.addEventListener('input', () => {
    n.setCustomValidity('');
    n.checkValidity();
  });

  n.addEventListener('invalid', () => {
  if (n.value === '') {
    n.setCustomValidity('Пожалуйста, укажите ваше имя')
  }
  else {
    n.setCustomValidity('Имя может содержать только строчные и заглавные буквы. Попробуйте снова')
  }
  })
}

/// PHONE VALIDATION ///
function phoneValidation(p) {
  p.addEventListener('input', () => {
    p.setCustomValidity('');
    p.checkValidity();
  });

  p.addEventListener('invalid', () => {
  if (p.value === '') {
    p.setCustomValidity('Пожалуйста, укажите номер вашего телефона')
  }
  else {
    p.setCustomValidity('Пожалуйста, введите номер в федеральном формате: +7 или 8, далее ваш номер.')
  }
  })
}

// FORM SUBMIT //

fbform.addEventListener('submit', (event) => {
  event.preventDefault();
  // shows success message
  $('#feedback .alert').show();
  let mail = new FormData(fbform);
  sendMail(mail);
  fbform.reset();
})

modform.addEventListener('submit', (event) => {
  event.preventDefault();
  $('#callModal .alert').show();
  let mail = new FormData(modform);
  sendMail(mail);
  modform.reset();
  $("#callModal").on("hidden.bs.modal", function () {
    $('#callModal .alert').hide();
  });
})

// sending forms data
const sendMail = (mail) => {
  fetch("/send", {
    method: "post",
    body: mail,
  }).then((response) => {
    return response.text();
  }).then((data) => {
    console.log(data);
  })
};

/////NEWS/////
const news_dates = Array.from(document.getElementsByClassName("showdate"));

let yesterday = (function(){this.setDate(this.getDate()-1); return this} )
          .call(new Date);
let twoweeks = (function(){this.setDate(this.getDate()-14); return this} )
          .call(new Date);
let threeweeks = (function(){this.setDate(this.getDate()-21); return this} )
          .call(new Date);

function formatDate(d){
  let month = String(d.getMonth() + 1);
  let day = String(d.getDate());
  let year = String(d.getFullYear());

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return `${day}.${month}.${year}`;
}

news_dates[0].textContent = formatDate(yesterday);
news_dates[1].textContent = formatDate(twoweeks);
news_dates[2].textContent = formatDate(threeweeks);
