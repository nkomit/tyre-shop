const express = require('express');
const app = express();
require("dotenv").config();
const port = 3000;

const nodemailer = require('nodemailer');
const multiparty = require('multiparty');

app.use(express.static('public'));

let mailConfig = {
    host: "smtp.ethereal.email",
    port: 587,
    auth: {
        user: process.env.USER,
        pass: process.env.PASS
    }
};

let transporter = nodemailer.createTransport(mailConfig);

transporter.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages");
  }
});

app.post("/send", (req, res) => {

  let form = new multiparty.Form();
  let data = {};
  form.parse(req, function (err, fields) {
    console.log(fields);
    Object.keys(fields).forEach(function (property) {
      data[property] = fields[property].toString();
    });

    const mail = {
      from: data.email,
      to: "jalon.kulas78@ethereal.email",
      text: `${data.name} <${data.email}> ${data.phone} \n${data.comment}`,
    };

    transporter.sendMail(mail, (err, data) => {
      if (err) {
        console.log(err);
        res.status(500).send("Something went wrong.");
      } else {
        res.status(200).send("Email successfully sent to recipient!");
      }
    });
  });
});


app.use(function(req, res, next) {
  res.status(404).sendFile(__dirname + '/public/404.html');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});
